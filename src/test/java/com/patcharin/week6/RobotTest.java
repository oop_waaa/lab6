package com.patcharin.week6;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEqauls("Robot", robot.getName());
        assertEqauls('R', robot.getSymbol());
        assertEqauls(10, robot.getx());
        assertEqauls(11, robot.gety());
    }


    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEqauls("Robot", robot.getName());
        assertEqauls('R', robot.getSymbol());
        assertEqauls(0, robot.getx());
        assertEqauls(0, robot.gety());
    }

    @Test
    public void shouldRobotupSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEqauls(true, result);
        assertEqauls(10, robot.gety());
    }


    @Test
    public void shouldRobotupfailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        boolean result = robot.up();
        assertEqauls(false, result);
        assertEqauls(Robot.Y_MIN, robot.gety());
    }

    @Test
    public void shouldRobotupNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEqauls(true, result);
        assertEqauls(6, robot.gety());
    }


    @Test
    public void shouldRobotupNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEqauls(true, result);
        assertEqauls(0, robot.gety());
    }

    
    @Test
    public void shouldRobotupNfail1() {
        Robot robot = new Robot("Robot", 'R', 10, 12);
        boolean result = robot.up(12);
        assertEqauls(false, result);
        assertEqauls(0, robot.gety());
    }


    @Test
    public void shouldRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEqauls(true, result);
        assertEqauls(10, robot.gety());
    }


    @Test
    public void shouldRobotDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(4);
        assertEqauls(true, result);
        assertEqauls(13, robot.gety());
    }


    @Test
    public void shouldRobotDownNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(9);
        assertEqauls(false, result);
        assertEqauls(19, robot.gety());
    }


    @Test
    public void shouldRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX-1);
        boolean result = robot.down();
        assertEqauls(true, result);
        assertEqauls(Robot.Y_MAX, robot.gety());
    }


    @Test
    public void shouldRobotDownAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        boolean result = robot.down();
        assertEqauls(false, result);
        assertEqauls(Robot.Y_MAX, robot.gety());
    }


    @Test
    public void shouldRobotleftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left();
        assertEqauls(true, result);
        assertEqauls(9, robot.getx());
    }



    @Test
    public void shouldRobotleftNFail1() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 10);
        boolean result = robot.left(5);
        assertEqauls(false, result);
        assertEqauls(Robot.X_MIN, robot.getx());
    }

    @Test
    public void shouldRobotleftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left(5);
        assertEqauls(true, result);
        assertEqauls(5, robot.getx());
    }


    @Test
    public void shouldRobotleftatfailAtMin() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 10);
        boolean result = robot.left();
        assertEqauls(false, result);
        assertEqauls(Robot.X_MIN, robot.getx());
    }


    @Test
    public void shouldRobotleftBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN+1, 10);
        boolean result = robot.left();
        assertEqauls(true, result);
        assertEqauls(Robot.X_MIN, robot.getx());
    }


    @Test
    public void shouldRobotrightSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right();
        assertEqauls(true, result);
        assertEqauls(11, robot.getx());
    }



    @Test
    public void shouldRobotrightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right(5);
        assertEqauls(true, result);
        assertEqauls(15, robot.getx());
    }


    
    @Test
    public void shouldRobotrightNFail1() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 10);
        boolean result = robot.right(5);
        assertEqauls(false, result);
        assertEqauls(Robot.X_MIN, robot.getx());
    }


    @Test
    public void shouldRobotrightAtMax() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 10);
        boolean result = robot.right();
        assertEqauls(false, result);
        assertEqauls(Robot.X_MAX, robot.getx());
    }

    
    @Test
    public void shouldRobrightBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX-1, 10);
        boolean result = robot.right();
        assertEqauls(true, result);
        assertEqauls(Robot.X_MAX, robot.getx());
    }


    private void assertEqauls(boolean b, boolean result) {
    }

    private void assertEqauls(String string, String string2) {
    }

    private void assertEqauls(int i, int j) {
    }

   

    
}
