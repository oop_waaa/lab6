package com.patcharin.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank patcharin = new BookBank("Patcharin", 100.0);      
        patcharin.print();
        patcharin.deposit(50);
        patcharin.print();
        patcharin.withdraw(50);
        patcharin.print();



        BookBank jk = new BookBank("Jungkook", 997.0);
        jk.deposit(10000000);
        jk.withdraw(100000000);
        jk.print();

        BookBank v = new BookBank("Teahyung", 995.0);
        v.deposit(10000000);
        v.withdraw(10);
        v.print();
    }
}
